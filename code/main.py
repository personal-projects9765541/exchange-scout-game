import sys

from action import Action

# Set your time_scaling (frequency of grapj updates)
Action.time_scale = 2000  # [ms]

if __name__ == "__main__":
    try:
        arg_1 = int(sys.argv[1])
    except:
        print("The first argument must be an integer.")

    match arg_1:
        case 1:  
            delaize = Action("Delaize", 10, 1, 1, 0.5)
            delaize.plot()
        case 2:
            colruyt = Action("Colruyt", 10, 1, 1, 0.5)
        case 3:
            aldi = Action("Aldi", 10, 1, 1, 0.5)
