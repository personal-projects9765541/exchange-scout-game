import threading
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


class Action:
    time_scale = 1
    timout_input = 5 * 60

    def __init__(self, name, initial_price=5, gamma_mean=1, gamma_deviation=1, hi_chance=0.5):
        self.name = name
        self.price = initial_price
        self.gamma_mean = gamma_mean
        self.gamma_deviation = gamma_deviation
        self.hi_chance = hi_chance
        self.lock = threading.Lock()
    
    def get_next_price_inc(self):
        ''' Calculate the next price of the action (at each time step) '''
        # Random choice of normal distribution
        hi = np.random.normal(self.gamma_mean, self.gamma_deviation)
        lo = np.random.normal(-self.gamma_mean, self.gamma_deviation)

        # Select hi or lo with a chance of hi_chance
        return hi if np.random.random() <= self.hi_chance else lo
    
    def update_properties_non_blocking(self):
        ''' Chnage the properties of the action '''
        while True:
            try:
                new_gamma_mean = float(input("Enter new gamma mean for the {} action, last value was {}: ".format(self.name, self.gamma_mean)))
            except:
                new_gamma_mean = self.gamma_mean
            try:
                new_gamma_deviation = float(input("Enter new gamma deviation for the {} action, last value was {}: ".format(self.name, self.gamma_deviation)))
            except:
                new_gamma_deviation = self.gamma_deviation
            try:
                new_hi_chance = float(input("Enter new hight chance for the {} action, last value was {}: ".format(self.name, self.hi_chance)))
            except:
                new_hi_chance = self.hi_chance
            with self.lock:
                self.gamma_mean      = float(new_gamma_mean)
                self.gamma_deviation = float(new_gamma_deviation)
                self.hi_chance       = float(new_hi_chance)
            print("")

    def summary(self, last_price):
        title       = "=== Summary of action {} ===\n".format(self.name)
        mean        = "\t--> Gamma mean:      {:.2f}\n".format(self.gamma_mean)
        deviation   = "\t--> Gamma deviation: {:.2f}\n".format(self.gamma_deviation)
        price       = "\t--> Current price:   {:.2f}\n".format(last_price)
        return title + mean + deviation + price

    def plot(self):
        '''
        Main function of the class. It plots the graph and update it. 
        It is also responsible for call the other methods  at good time.
        '''
        fig = plt.figure()
        x = [i for i in range(100)]
        y = [self.get_next_price_inc() + self.price for _ in range(100)]

        def animate(i):
            plt.clf()
            y.pop(0)
            y.append(np.mean(y[-7:]) + self.get_next_price_inc())
            plt.plot(x, y, color='g')
            plt.title("Evolution of the price of the {} action".format(self.name))
            plt.xlabel("Time")
            plt.ylabel("Price")

            # Change factor every 20 * 30 timescale
            if (i % (30) == 0) and i > 15:
                self.summary(np.mean(y[-7:]))

        user_input_thread = threading.Thread(target=self.update_properties_non_blocking)
        user_input_thread.daemon = True
        user_input_thread.start()
            
        # Plot
        ani = animation.FuncAnimation(fig, animate, interval=self.time_scale, save_count=0, cache_frame_data=False)
        plt.title("Evolution of the price of the {} action".format(self.name))
        plt.xlabel("Time")
        plt.ylabel("Price")
        plt.show() 
