# Basic rules of the game

## Objective
Each time have to get as much money as possible. To do so, they can buy actions in the bank, wait for the action price to increase and sell it. Another possibility is to work, and get a fixed amount of money each time they work, but this can be quite long, and not very efficient.

## Actions
The price of the action will not always increase. In fact, it dempends on the action model. Each action has its own model, with its own parameters. 

### Mathematical model
At each time step the price of the action is computed using the following formula:
$$ p_{t+1} = \frac{\sum_{i=0}^6 p_{t-i}}{7} + \mathcal{G}(\mu, \sigma) $$
where $p_t$ is the price of the action at time $t$, $\mathcal{G}(\mu, \sigma)$ is either $\mathcal{N}(\mu, \sigma)$ or $\mathcal{N}(-\mu, \sigma)$. The choice is made randomly, with a probability of $h_i$ for the first option.

The parameters $\mu$ and $\sigma$ are the mean and standard deviation of the normal distribution, and $h_i$ is the probability of having a positive evolution of the action price at time $t + 1$.

### Parameters
The parameters of the action model are:
- $\mu$: the mean of the normal distribution
- $\sigma$: the standard deviation of the normal distribution
- $h_i$: the probability of having a positive evolution of the action price at time $t + 1$

### Evolution
The parameters of the action model can be modified by the bank. The bank can always change both of these 3 parameters, to control the evolution of the action price.

## Game evolution
At the beginning of the game, each group has a certain amount of money. Than, they can go to some person to performs some actions.

### By an action
The group can go to the bank to buy an action. The price of the action is computed using the action model. When a group buys an action, the price of the action is set to the current price displayed on the action graph. The group will thus pay cash to the bank, and recieve an action ticket in return.

### Sel an action
The group can go to the bank to sell an action. The price of the action is computed using the action model. When a group sells an action, the price of the action is set to the current price displayed on the action graph. The group will thus recieve cash from the bank, and give an action ticket in return.

### Work
A group can go to a place to do some work. Each time they work, they will recieve a fixed amount of money. This amount is fixed by the employer, and can be different for each employer. The group will thus recieve cash from the employer.

### Trader
A group can go to a trader to get some information about the action model. The trader will give them the parameters of the action model. The group will thus pay cash to the hacker.

### Hacker
A group can go to a hacker to pay it to hack the bank and modify one of the action model parameters. The hacker will thus modify one of the parameters of the action model. The group will thus pay cash to the hacker. (In fact, the hacker send a message to the bank to modify the action model parameters, but the group doesn't have to know that)

## End of the game
The game ends when the time is over. The group with the most money wins the game.