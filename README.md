# Exchange Scout game

## Description
This is a sccout game, dedigned to be played in groups. Each group will have as goal to get as many money as possible.
To get money, they can by an action in the bank, wait for the action price to increase and sell it.
But, the price of the action will increase (or decrease) randomly, and the group will have to decide when to sell the action. Here is an example of the price evolution of an action:
<p align="center"><img src="img/action_exemple.png" alt="Exemple of the price evolution of an action" width="50%"/></p>

To add interation, it this possible for the bank to modify the action models parameters (mean, standard deviation and increase/decrease random focator) to make the game more difficult. It's thus possible to add interation between the groups, as they will have to negociate with the bank to get the best parameters for they own actions and the worst for the other groups.

To get more information about the rules of the game, please read the [rules](rules/rules.md) file.

## Installation
This repository contains a *code* folder when the python code of the game is stored, and a *rules* folder when the rules of the game are stored.

### Packages
To run the game, you will need to install the following python packages:
- numpy
- threading
- matplotlib

To install them, you can use the following command:
```bash
pip install numpy threading matplotlib
```

### Run the game
1. Clone the repository
2. Go to the *code* folder
3. Adapt the *main.py* file to your needs (by adding actions or not and by changing the parameters of the game)
4. Run the *main.py* file
```bash
python3 main.py <action number>
```
You can lauch mutliple instances of the game by running the *main.py* file in multiple terminals and changing the firt parameter (action number) to have different actions in terminal. Each action number must be unique.

## Contributing
If you want to contribute to this project, you can do several things:
- Contributing to the code, by adding new features or fixing bugs. Please create a pull request to do so, and explain what you did.
- Contributing to the rules, by adding new rules or fixing mistakes. Please create a pull request to do so,. You can also create new variants of the game, and add a new file named *rules-variant_name.md*, in the *rules* folder.